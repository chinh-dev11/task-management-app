import type { Config } from 'tailwindcss'

const config: Config = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  presets: [require('./src/styles/appPresets')],
  // presets: [], // without any presets, including Tailwind's.
  theme: {},
  plugins: [],
}
export default config
