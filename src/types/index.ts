export interface User {
  id: string
  permission: string
  name: string
  username: string
}
export interface Task {
  id: string
  title: string
  description: string
  createdAt: string
  endAt: string
  status: string
  userId: string
}

export enum Permission {
  Admin = 'admin',
  Owner = 'owner',
}

export enum Status {
  Pending = 'pending',
  Completed = 'completed',
  Overdue = 'overdue',
}

export interface RouteLink {
  key: string
  path: string
}

export enum ButtonBgText {
  Purple = 'purple',
  Red = 'red',
  Gray = 'gray',
  DarkGray = 'dark-gray',
  Light = 'light',
  White = 'white',
}

export enum Direction {
  Asc = 'asc',
  Desc = 'desc',
  Boolean = 'boolean',
}

export interface OrderDirection {
  Asc?: 'asc'
  Desc?: 'desc'
  Boolean?: 'boolean'
}

export enum SortIndex {
  DueDate = 'endAt',
  Title = 'title',
}
