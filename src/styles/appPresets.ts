module.exports = {
  darkMode: 'class',
  theme: {
    extend: {
      colors: {
        'app-dark': {
          900: 'rgb(var(--app-dark-900) / <alpha-value>)',
          800: 'rgb(var(--app-dark-800) / <alpha-value>)',
          700: 'rgb(var(--app-dark-700) / <alpha-value>)',
          DEFAULT: 'rgb(var(--app-dark-default) / <alpha-value>)', // darkest
        },
        'app-light': {
          300: 'rgb(var(--app-light-300) / <alpha-value>)',
          200: 'rgb(var(--app-light-200) / <alpha-value>)',
          100: 'rgb(var(--app-light-100) / <alpha-value>)',
          DEFAULT: 'rgb(var(--app-light-default) / <alpha-value>)', // lightest
        },
        'app-purple': {
          DEFAULT: 'rbg(var(--app-purple) / <alpha-value>)',
        },
        app: {
          'text-error': 'rgb(var(--app-red) / <alpha-value>)',
        },
      },
      width: {
        'app-width-sm': 'var(--app-width-sm)',
        'app-width-md': 'var(--app-width-md)',
        'app-width-lg': 'var(--app-width-lg)',
        'app-width-sidebar': 'var(--app-width-sidebar)',
      },
      minWidth: {
        'app-min-width': 'var(--app-min-width)', // mobile
      },
      maxWidth: {
        'app-max-width': 'var(--app-max-width)', // mobile
      },
      height: {
        'app-height-sidebar': 'var(--app-height-sidebar)', // 4rem - h-16
      },
    },
  },
  plugins: [require('@tailwindcss/forms')],
}
