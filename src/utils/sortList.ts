import moment, { MomentInput } from 'moment'
import { Direction, OrderDirection } from '@/types'
import { orderBy as _orderBy } from 'lodash'

export const sortList = <T>(
  list: T[],
  orderBy: keyof T | string,
  direction: OrderDirection
): T[] => {
  // Convert `orderBy` to "asc" or "desc" if it's a boolean
  const orderDirection =
    typeof direction === Direction.Boolean
      ? direction
        ? Direction.Asc
        : Direction.Desc
      : direction

  // Validate that 'orderDirection' is either 'asc' or 'desc'
  if (orderDirection !== Direction.Asc && orderDirection !== Direction.Desc) {
    throw new Error(
      `Invalid order direction. Must be '${Direction.Asc}' or '${Direction.Desc}'.`
    )
  }
  return _orderBy(
    list,
    (item: T) => {
      const itemValue = item[orderBy as keyof T]
      return moment(itemValue as MomentInput)
    },
    [orderDirection]
  )
}
