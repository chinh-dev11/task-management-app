export const generateRandomNumbers = (length = 4): string => {
  let numbers = ''

  for (let i = 0; i < length; i++) {
    numbers += Math.floor(Math.random() * 10)
  }
  return numbers
}
