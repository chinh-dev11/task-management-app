import { generateRandomNumbers } from './generateRandomNumbers'

export const generateTaskId = (): string => `task-${generateRandomNumbers()}`
