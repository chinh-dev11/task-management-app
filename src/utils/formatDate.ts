import moment, { Moment } from 'moment'

export const formatDate = (
  date: string | Moment,
  format: string = 'YYYY-MM-DD'
): string => {
  return moment(date).format(format)
}
