import { formatDate } from './formatDate'
import moment from 'moment'

export const todayDate = () => formatDate(moment())
