import { ReactNode } from 'react'
import { ButtonBgText } from '@/types'
import cn from 'classnames'

interface AppButtonIconProps {
  onClick: () => void
  children: ReactNode | null
  textBg?: string
  width?: string
  type?: 'button' | 'submit' | 'reset'
  disabled?: boolean
}

const AppButtonIcon = ({
  onClick,
  children,
  textBg = ButtonBgText.Gray,
  width = '',
  type = 'button',
  disabled = false,
}: AppButtonIconProps) => {
  const hover = disabled ? '' : 'hover'
  const css = 'flex items-center py-2 pr-6 pl-2 ml-2 rounded-full'
  const cssDynamic = `app-bg-${textBg} app-bg-${textBg}-text ${hover} ${width}`

  return (
    <button
      onClick={onClick}
      type={type}
      className={cn(css, cssDynamic)}
      disabled={disabled}
    >
      {children}
    </button>
  )
}

export default AppButtonIcon
