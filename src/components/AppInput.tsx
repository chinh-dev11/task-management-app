import { ChangeEvent } from 'react'
import cn from 'classnames'
import { MdOutlineCancel } from 'react-icons/md'

interface AppInputProps {
  item: {
    label?: string
    type?: string
    name: string
    value: string
    placeholder?: string
  }
  multiline?: boolean
  onChange: (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => void
  error?: string
}

export default function AppInput({
  item: { label, type = 'text', name, value, placeholder },
  multiline = false,
  onChange,
  error = '',
}: AppInputProps) {
  const cssClasses =
    'rounded-md border-app-light-100 dark:border-gray-500 w-full dark:bg-app-dark-700 dark:text-app-light-200'

  return (
    <div className="w-full">
      <label className="flex justify-between" htmlFor={name}>
        <span className="text-app-light-200 mb-1 dark:text-app-light-300">
          {label}
        </span>
        {error && (
          <span className="text-app-text-error">
            <MdOutlineCancel className="inline mr-1" />
            {error}
          </span>
        )}
      </label>
      {multiline ? (
        <textarea
          name={name}
          value={value}
          placeholder={placeholder}
          onChange={onChange}
          className={cn(cssClasses, 'min-h-[10rem] max-h-[20rem] resize-y')}
        />
      ) : (
        <input
          type={type}
          name={name}
          value={value}
          placeholder={placeholder}
          onChange={onChange}
          className={`${cssClasses}`}
        />
      )}
    </div>
  )
}
