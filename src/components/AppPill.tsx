interface AppStatusProps {
  status: string
}

const AppPill = ({ status }: AppStatusProps) => {
  const bg = `app-status-pill-${status}`
  const dot = `app-status-dot-${status}`

  return (
    <div className={bg}>
      <span className={dot} />
      {status}
    </div>
  )
}

export default AppPill
