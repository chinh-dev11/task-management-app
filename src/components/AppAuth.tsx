'use client'

import { useEffect } from 'react'
import { redirect } from 'next/navigation'
import { useAppSelector } from '@/store/hooks'
import { ROUTES } from '@/const'

/**
 * HOC to protect routes that require authentication.
 * Non authenticated users will be redirected to login page.
 *
 * @param WrappedComponent
 * @returns Component of protected routes.
 */
export const AppAuth = (WrappedComponent: any) => {
  return function AppAuth(props: any) {
    const currentUser = useAppSelector((state) => state.users.currentUser)

    useEffect(() => {
      if (!currentUser) {
        redirect(ROUTES.login.path)
      }
    }, [currentUser])

    if (!currentUser) {
      return null
    }

    return <WrappedComponent {...props} />
  }
}
