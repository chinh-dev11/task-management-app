/**
 * TODO: How to efficiently create modals within Next.js framework.
 * Sources:
 *    - https://nextjs.org/docs/app/building-your-application/routing/intercepting-routes#modals
 *    - https://nextjs.org/docs/app/building-your-application/routing/parallel-routes#modals
 */

interface AppOverlayProps {
  onClick?: () => void
}

export default function AppOverlay({ onClick }: AppOverlayProps) {
  return (
    <div
      onClick={onClick}
      className="fixed top-0 left-0 w-full h-full cursor-pointer z-50 opacity-40 bg-black"
      data-testid="appOverlay"
    />
  )
}
