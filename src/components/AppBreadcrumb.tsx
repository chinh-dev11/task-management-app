import Link from 'next/link'
import { RouteLink } from '@/types'
import { ReactNode } from 'react'

interface AppBreadcrumbProps {
  link?: RouteLink | null
  children?: ReactNode
}

export default function AppBreadcrumb({ link, children }: AppBreadcrumbProps) {
  return (
    <div className="px-5 py-7 bg-app-light dark:bg-app-dark-900">
      {link && (
        <Link key={link.key} href={link.path}>
          {children}
        </Link>
      )}
      {!link && <div>{children}</div>}
    </div>
  )
}
