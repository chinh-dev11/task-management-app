'use client'
import { ROUTES } from '@/const'
import { useAppSelector } from '@/store/hooks'
import Image from 'next/image'
import { MdOutlineWbSunny } from 'react-icons/md'
import Link from 'next/link'
import { User } from '@/types'

const AppSidebar = () => {
  const currentUser: User | null = useAppSelector(
    (state) => state.users.currentUser
  )
  const toggleTheme = () => {
    document.documentElement.classList.toggle('dark')
  }

  return (
    <>
      <div className="lg:hidden text-white fixed z-30 w-screen overflow-y-scroll">
        <div className="flex justify-between items-center min-w-app-min-width max-w-app-max-width h-app-height-sidebar bg-app-dark-800">
          <Link
            key={ROUTES.home.key}
            href={ROUTES.home.path}
            className="bg-app-light p-3 ml-2"
          >
            <Image
              src="/next.svg"
              alt="logo"
              width={10}
              height={10}
              priority={true}
              className="w-10 h-auto"
            />
          </Link>
          <div className="flex gap-8 px-4 py-3">
            <button
              onClick={toggleTheme}
              className="text-app-light-200 dark:text-gray-500"
            >
              <MdOutlineWbSunny />
            </button>
            <div className="flex gap-3 items-center">
              <span className="text-app-light-200 dark:text-gray-500">
                {currentUser?.name}
              </span>
              <div className="">
                <Image
                  src="/user-placeholder.png"
                  width={30}
                  height={30}
                  alt="avatar image"
                  priority={true}
                  className="inline-block rounded-full"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="hidden lg:block text-white fixed z-30 bg-app-dark-800 w-app-width-sidebar">
        <div className="grid grid-cols-1 content-between h-screen bg-app-dark-800">
          <Link
            key={ROUTES.home.key}
            href={ROUTES.home.path}
            className="bg-app-light p-1 m-2"
          >
            <Image
              src="/next.svg"
              width={10}
              height={10}
              alt="logo"
              priority={true}
              className="w-auto"
            />
          </Link>
          <div className="text-center">
            <button
              onClick={toggleTheme}
              className="p-8 pb-10 mb-10 border-gray-700 border-b"
            >
              <MdOutlineWbSunny />
            </button>
            <div className="flex flex-col gap-3 items-center">
              <span className="lg:hidden">{currentUser?.name}</span>
              <div className="mb-8">
                <Image
                  src="/user-placeholder.png"
                  width={30}
                  height={30}
                  alt="avatar image"
                  priority={true}
                  className="inline-block rounded-full"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default AppSidebar
