import { FormEvent, ReactNode } from 'react'
import { ButtonBgText } from '@/types'
import cn from 'classnames'

interface AppButtonProps {
  onClick?: (e: FormEvent<HTMLFormElement>) => void
  children: ReactNode | null
  bg?: string
  color?: string
  textBg?: string
  width?: string
  type?: 'button' | 'submit' | 'reset'
  disabled?: boolean
  name: string
}

const AppButton = ({
  onClick,
  children,
  textBg = ButtonBgText.Gray,
  width = '',
  type = 'button',
  disabled = false,
  name,
}: AppButtonProps) => {
  const hover = disabled ? '' : 'hover'
  const css = 'py-3 px-6 ml-2 rounded-full'
  const cssDynamic = `dark:app-bg-${textBg} app-bg-${textBg} app-bg-${textBg}-text hover:bg-app-bg-${textBg} ${hover} ${width}`

  return (
    <button
      onClick={(e: any) => {
        if (onClick) onClick(e)
      }}
      type={type}
      className={cn(css, cssDynamic)}
      disabled={disabled}
      name={name}
    >
      {children}
    </button>
  )
}

export default AppButton
