import { ReactNode } from 'react'

interface AppHeaderProps {
  children?: ReactNode
}

const AppHeader = ({ children }: AppHeaderProps) => {
  return (
    <header className="bg-white dark:bg-app-dark-800 py-5 px-4 mb-8 mx-5 rounded-md max-w-app-max-width">
      {children}
    </header>
  )
}

export default AppHeader
