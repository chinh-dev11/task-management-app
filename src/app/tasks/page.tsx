'use client'

import Tasks from './Tasks'
import { AppAuth } from '@/components/AppAuth'

const WrappedComponent = () => <Tasks />

export default AppAuth(WrappedComponent)
