import { SortIndex } from '@/types'
import cn from 'classnames'

interface SortWithButtonProps {
  handleClick: (by: string) => void
  sortState: {
    endAt?: boolean
  }
}

export default function SortWithButton({
  handleClick,
  sortState,
}: SortWithButtonProps) {
  const bg = sortState.endAt ? 'bg-app-light dark:bg-app-dark-900' : ''

  return (
    <div className="px-5">
      <div className="flex items-center gap-2 bg-white dark:bg-app-dark-800 px-5 py-4 mb-2 rounded-md max-w-app-max-width text-sm">
        <div className="text-app-light-200 dark:text-gray-500">Sort By</div>
        <button
          onClick={() => handleClick(SortIndex.DueDate)}
          className={cn(
            bg,
            'border border-app-light dark:border-gray-500 px-2 py-1 rounded-md'
          )}
        >
          Due Date
        </button>
        <button
          onClick={() => handleClick(SortIndex.Title)}
          disabled
          className="border border-app-light dark:border-gray-500 px-2 py-1 rounded-md"
        >
          Status
        </button>
      </div>
    </div>
  )
}
