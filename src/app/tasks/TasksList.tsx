import { Task } from '@/types'
import TaskItem from '@/app/tasks/[taskId]/TaskItem'
import TaskCard from '@/app/tasks/[taskId]/TaskCard'

interface ListProps {
  list: Task[]
}

export default function List({ list }: ListProps) {
  return (
    <ul className="flex flex-col gap-2">
      {list.map((item: Task) => (
        <li key={item.id} data-testid="listItem">
          <div className="hidden md:block">
            <TaskItem item={item} />
          </div>
          <div className="md:hidden">
            <TaskCard item={item} />
          </div>
        </li>
      ))}
    </ul>
  )
}
