import { Task, User } from '@/types'
import { useAppSelector } from '@/store/hooks'

interface ItemProps {
  item: Task | null
}

export default function Detail({ item }: ItemProps) {
  const usersState: User[] = useAppSelector((state) => state.users.users)
  const currentUser =
    usersState.find(({ id }) => id === item?.userId)?.name || 'unknown'

  return (
    <div className=" w-full">
      <div className="flex justify-between">
        <div className="flex gap-2 items-baseline">
          <div className="text-app-light-200 dark:text-gray-500">Due Date</div>
          <div className=" text-xl font-bold">{item?.endAt}</div>
        </div>
        <div className="text-right">
          <div>
            <span className="text-app-light-200 dark:text-gray-500 mr-2">
              User
            </span>
            <span className="font-bold">{currentUser}</span>
          </div>
          <div>
            <span className="text-app-light-200 dark:text-gray-500"># </span>
            <span>{item?.id}</span>
          </div>
        </div>
      </div>
      <div className="app-bg-lightest dark:bg-app-dark-700 my-10 py-10 px-5 rounded-md">
        <div className="flex gap-6 mb-5">
          <div className="w-[20%] text-app-light-200 dark:text-gray-500 text-right">
            Title
          </div>
          <p className="w-[80%] app-truncate-long-word" title={item?.title}>
            {item?.title}
          </p>
        </div>
        <div className="flex gap-6">
          <div className="w-[20%] text-app-light-200 dark:text-gray-500 text-right">
            Description
          </div>
          <p
            className="w-[80%] app-truncate-long-word"
            title={item?.description}
          >
            {item?.description}
          </p>
        </div>
      </div>
    </div>
  )
}
