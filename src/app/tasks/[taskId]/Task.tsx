'use client'

import { useRouter } from 'next/navigation'
import { useAppSelector, useAppDispatch } from '@/store/hooks'
import TaskDetail from '@/app/tasks/[taskId]/TaskDetail'
import { ROUTES } from '@/const'
import AppBreadcrumb from '@/components/AppBreadcrumb'
import AppHeader from '@/components/AppHeader'
import { useState } from 'react'
import { ButtonBgText, Status, Task as TaskType } from '@/types'
import { setTasksState } from '@/store/features/tasks/tasksSlice'
import AppOverlay from '@/components/AppOverlay'
import TaskForm from '@/app/tasks/[taskId]/TaskForm'
import { MdArrowBackIosNew } from 'react-icons/md'
import AppPill from '@/components/AppPill'
import AppButton from '@/components/AppButton'
import { useFormik } from 'formik'
import { taskFormSchema } from '@/validation'

export default function Task(params: { taskId: string }) {
  const tasksState = useAppSelector((state) => state.tasks.value)
  const dispatch = useAppDispatch()

  const currentTaskIndex = tasksState.findIndex(
    (task: TaskType) => task.id === params.taskId
  )
  const router = useRouter()

  const [showForm, setShowForm] = useState(false)
  const [showConfirmation, setShowConfirmation] = useState(false)

  const formik = useFormik({
    initialValues: { ...tasksState[currentTaskIndex] },
    validationSchema: taskFormSchema,
    onSubmit: (values) => saveForm(values),
  })

  const handleEdit = () => {
    formik.resetForm({ values: { ...tasksState[currentTaskIndex] } })
    setShowForm(true)
  }

  const saveForm = (values: TaskType) => {
    // edit task
    const editedTasks = [...tasksState]
    editedTasks.splice(currentTaskIndex, 1) // remove task
    dispatch(setTasksState([values, ...editedTasks])) // prepend to the task list
    closeForm()
  }

  const handleDelete = () => {
    const editedTasks = [...tasksState]
    editedTasks.splice(currentTaskIndex, 1) // remove task
    dispatch(setTasksState([...editedTasks]))
    closeConfirmation()
    router.push(ROUTES.tasks.path) // go back to task list
  }

  const handleMarkAsCompleted = () => {
    const taskCompleted = {
      ...tasksState[currentTaskIndex],
      status: Status.Completed,
    }
    const tasksRemoved = [...tasksState]
    tasksRemoved.splice(currentTaskIndex, 1)
    dispatch(setTasksState([taskCompleted, ...tasksRemoved]))
    closeConfirmation()
  }

  const closeForm = () => {
    setShowForm(false)
    formik.resetForm()
  }

  const closeConfirmation = () => {
    setShowConfirmation(false)
  }

  return (
    <div className="w-screen">
      <main className="app-main">
        <div className="sticky top-0">
          <div className="app-min-max-w bg-app-light dark:bg-app-dark-900">
            <AppBreadcrumb link={ROUTES.tasks}>
              <div className="flex items-center gap-2">
                <span className="app-text-purple">
                  <MdArrowBackIosNew />
                </span>
                <span>Go Back</span>
              </div>
            </AppBreadcrumb>
            <AppHeader>
              <div className="flex justify-between items-center">
                <div className="flex items-center gap-2">
                  <span className="text-app-light-200 dark:text-gray-500">
                    Status
                  </span>
                  {tasksState[currentTaskIndex]?.status && (
                    <AppPill status={tasksState[currentTaskIndex].status} />
                  )}
                </div>
                <div className="text-white flex items-center gap-2">
                  <AppButton
                    textBg={ButtonBgText.DarkGray}
                    onClick={handleEdit}
                    name="edit"
                  >
                    Edit
                  </AppButton>
                  <AppButton
                    textBg={ButtonBgText.Red}
                    onClick={() => setShowConfirmation(true)}
                    name="delete"
                  >
                    Delete
                  </AppButton>
                  <AppButton
                    textBg={ButtonBgText.Purple}
                    onClick={handleMarkAsCompleted}
                    name="markAsCompleted"
                    disabled={
                      tasksState[currentTaskIndex]?.status === Status.Completed
                    }
                  >
                    Mark Completed
                  </AppButton>
                </div>
              </div>
            </AppHeader>
          </div>
        </div>
        <section className="px-5">
          <div className=" bg-white dark:bg-app-dark-800 rounded-md px-10 py-10">
            <TaskDetail item={tasksState[currentTaskIndex]} />
          </div>
        </section>
      </main>
      {showForm && (
        <>
          <AppOverlay onClick={closeForm} />
          <div className="app-overlay-content-left lg:left-app-width-sidebar bg-app-light dark:bg-app-dark-900 rounded-br-md p-10 shadow-xl rounded-r-2xl">
            <TaskForm formik={formik}>
              <div className="flex gap-2 justify-end py-7">
                <AppButton onClick={closeForm} name="cancel">
                  Cancel
                </AppButton>
                <AppButton
                  onClick={formik.handleSubmit}
                  textBg={ButtonBgText.Purple}
                  name="submit"
                  disabled={formik.isSubmitting}
                >
                  Save
                </AppButton>
              </div>
            </TaskForm>
          </div>
        </>
      )}
      {showConfirmation && (
        <>
          <AppOverlay onClick={closeConfirmation} />
          <div className="app-overlay-content-center">
            <div className="bg-app-light dark:bg-app-dark-700 w-app-overlay-container-width rounded-md p-8">
              <div className="mb-5">
                <h2 className="mb-5">Confirm Deletion</h2>
                <div>
                  Are you sure you want to delete task{' '}
                  <p className="app-truncate-long-word mb-2">
                    <span className="font-bold">
                      {tasksState[currentTaskIndex]?.title}
                    </span>
                    <span> ?</span>
                  </p>
                  <p>This action cannot be undone.</p>
                </div>
              </div>
              <div className="text-right">
                <AppButton onClick={closeConfirmation} name="cancel">
                  Cancel
                </AppButton>
                <AppButton
                  onClick={handleDelete}
                  textBg={ButtonBgText.Red}
                  name="delete"
                >
                  Delete
                </AppButton>
              </div>
            </div>
          </div>
        </>
      )}
    </div>
  )
}
