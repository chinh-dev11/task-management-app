import AppInput from '@/components/AppInput'
import { Status, Task } from '@/types'
import { ReactNode } from 'react'
import { FormikProps } from 'formik'

interface ItemProps {
  item?: Task
  children?: ReactNode
  formik: FormikProps<Task>
}

export default function Form({ item, children, formik }: ItemProps) {
  return (
    <>
      <div className="mb-5">
        {formik.values.id && (
          <>
            <h1 className="mb-1">Task</h1>
            <div>
              <span className="text-app-light-200 mr-1">#</span>
              <span>{formik.values.id}</span>
            </div>
          </>
        )}
        {!formik.values.id && <h1 className="mb-5">New Task</h1>}
      </div>
      <form noValidate className="">
        <div>
          <div className="mb-3">
            <AppInput
              item={{
                label: 'Title',
                name: 'title',
                value: formik.values.title,
              }}
              onChange={formik.handleChange}
              error={formik.errors.title}
            />
          </div>
          <div className="mb-3">
            <AppInput
              item={{
                label: 'Description',
                name: 'description',
                value: formik.values.description,
              }}
              multiline={true}
              onChange={formik.handleChange}
              error={formik.errors.description}
            />
          </div>
          <div className="mb-3">
            <AppInput
              item={{
                label: 'Due Date',
                type: 'date',
                name: 'endAt',
                value: formik.values.endAt,
              }}
              onChange={formik.handleChange}
              error={formik.errors.endAt}
            />
          </div>
        </div>
        {formik.values.status && (
          <>
            <label className="block text-app-light-200 mb-2" htmlFor="status">
              Status
            </label>
            <select
              onChange={formik.handleChange}
              name="status"
              defaultValue={formik.values.status}
              className="w-full rounded-md border-app-light-100 capitalize dark:bg-app-dark-700"
            >
              <option value={Status.Pending}>{Status.Pending}</option>
              <option value={Status.Completed}>{Status.Completed}</option>
              {formik.values.status === Status.Overdue && (
                <option value={Status.Overdue} disabled>
                  {Status.Overdue}
                </option>
              )}
            </select>
          </>
        )}
        <div>{children}</div>
      </form>
    </>
  )
}
