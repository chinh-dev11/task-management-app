'use client'

import Task from './Task'
import { AppAuth } from '@/components/AppAuth'

const WrappedComponent = ({ params }: { params: { taskId: string } }) => (
  <Task {...params} />
)

export default AppAuth(WrappedComponent)
