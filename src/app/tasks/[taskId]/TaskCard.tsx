import { Task } from '@/types'
import Link from 'next/link'
import { MdArrowForwardIos } from 'react-icons/md'
import AppPill from '@/components/AppPill'
import { ROUTES } from '@/const'

interface CardProps {
  item: Task
}

export default function Card({ item }: CardProps) {
  return (
    <div className="flex p-5 justify-between gap-5 bg-white dark:bg-app-dark-800 rounded-md">
      <div className="w-[55%]">
        <div className="mb-2 flex items-top">
          <div className="text-app-light-200 dark:text-gray-500 mr-2 inline-block">
            Title
          </div>
          <p
            className="font-bold inline-block app-truncate-long-word"
            title={item.title}
          >
            {item.title}
          </p>
        </div>
        <div>
          <div className="text-app-light-200 dark:text-gray-500 mb-1">
            Description
          </div>
          <p className="app-truncate-long-word" title={item.description}>
            {item.description}
          </p>
        </div>
      </div>
      <div className="w-[35%]">
        <div className="mb-3">
          <span className="text-app-light-200 dark:text-gray-500 mr-2">
            Due Date
          </span>
          <span className="font-bold">{item.endAt}</span>
        </div>
        <div className="">
          <AppPill status={item.status} />
        </div>
      </div>
      <div className="w-[3%]">
        <Link
          key={item.id}
          href={`${ROUTES.tasks.path}/${item.id}`}
          className="flex items-center justify-center h-full"
        >
          <MdArrowForwardIos />
        </Link>
      </div>
    </div>
  )
}
