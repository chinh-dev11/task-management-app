import { Task } from '@/types'
import Link from 'next/link'
import { MdArrowForwardIos } from 'react-icons/md'
import AppPill from '@/components/AppPill'
import { ROUTES } from '@/const'

interface ItemProps {
  item: Task
}

export default function Item({ item }: ItemProps) {
  return (
    <div className="flex gap-4 items-center px-5 py-2 bg-white dark:bg-app-dark-800 rounded-md">
      <p
        className="w-[20%] font-bold app-truncate-long-word"
        title={item.title}
      >
        {item.title}
      </p>
      <p className="w-[40%] app-truncate-long-word" title={item.description}>
        {item.description}
      </p>
      <div className="w-[17%]">{item.endAt}</div>
      <div className="w-[20%] flex flex-row justify-center">
        <AppPill status={item.status} />
      </div>
      <div className="w-[3%] text-right">
        <Link
          key={item.id}
          href={`${ROUTES.tasks.path}/${item.id}`}
          className="inline-block"
        >
          <MdArrowForwardIos />
        </Link>
      </div>
    </div>
  )
}
