import { MdArrowDropDown } from 'react-icons/md'
import TaskSortWithIcon from '@/app/tasks/TasksSortWithIcon'

interface ListHeaderProps {
  handleClick: (by: string) => void
  sortState: {
    endAt?: boolean
  }
}

export default function ListHeader({
  handleClick,
  sortState,
}: ListHeaderProps) {
  return (
    <div className="px-5">
      <div className="flex gap-4 items-center bg-white p-5 mb-2 rounded-md max-w-app-max-width text-app-light-200 dark:text-gray-500 dark:bg-app-dark-700">
        <div className="w-[20%]">Title</div>
        <div className="w-[40%]">Description</div>
        <div className="w-[17%] flex items-center">
          <span className="inline-block">Due Date</span>
          <TaskSortWithIcon
            handleClick={handleClick}
            sortState={sortState}
            type="icon"
          >
            <MdArrowDropDown className="size-8" />
          </TaskSortWithIcon>
        </div>
        <div className="w-[20%] text-center">Status</div>
        <div className="w-[3%]"></div>
      </div>
    </div>
  )
}
