'use client'

import { useAppSelector, useAppDispatch } from '@/store/hooks'
import {
  ButtonBgText,
  Direction,
  OrderDirection,
  Task,
  User,
  Status,
  SortIndex,
} from '@/types'
import { useEffect, useState } from 'react'
import TasksListHeader from '@/app/tasks/TasksListHeader'
import TasksList from '@/app/tasks/TasksList'
import AppOverlay from '@/components/AppOverlay'
import TaskForm from '@/app/tasks/[taskId]/TaskForm'
import { generateTaskId, todayDate, sortList } from '@/utils'
import { setTasksState } from '@/store/features/tasks/tasksSlice'
import AppBreadcrumb from '@/components/AppBreadcrumb'
import AppHeader from '@/components/AppHeader'
import AppInput from '@/components/AppInput'
import { MdOutlineSearch } from 'react-icons/md'
import { PiPlus } from 'react-icons/pi'
import AppButtonIcon from '@/components/AppButtonIcon'
import AppButton from '@/components/AppButton'
import { DEFAULT_TASK } from '@/const'
import TasksSortWithButton from '@/app/tasks/TasksSortWithButton'
import { useFormik } from 'formik'
import { taskFormSchema } from '@/validation'

export default function Tasks() {
  const tasksState: Task[] = useAppSelector((state) => state.tasks.value)
  const currentUser: User | null = useAppSelector(
    (state) => state.users.currentUser
  )
  const dispatch = useAppDispatch()

  const [showForm, setShowForm] = useState(false)
  const [searchTerm, setSearchTerm] = useState('')
  const [tasksList, setTasksList] = useState<Task[]>(tasksState)
  const [btnDueDate, setBtnDueDate] = useState(false)
  const [sortState, setSortState] = useState({})

  useEffect(() => {
    setSortState({ endAt: btnDueDate })

    if (btnDueDate) {
      const sorted = sortList<Task>(
        tasksList,
        SortIndex.DueDate,
        Direction.Desc as unknown as OrderDirection
      )
      setTasksList(sorted)
    } else {
      setTasksList(tasksState)
    }
  }, [btnDueDate])

  const handleSearch = () => {
    console.log('handleSearch', searchTerm)
  }

  const saveForm = (values: Task) => {
    // new task creation
    const task: Task = {
      ...values,
      id: generateTaskId(),
      createdAt: todayDate(),
      status: Status.Pending,
      userId: currentUser?.id || '',
    }
    dispatch(setTasksState([task, ...tasksList])) // prepend to the task list
    closeForm()
  }

  const closeForm = () => {
    setShowForm(false)
    formik.resetForm()
  }

  const handleSortBy = (by: string) => {
    if (by === SortIndex.DueDate) setBtnDueDate(!btnDueDate)
  }

  const formik = useFormik({
    initialValues: { ...DEFAULT_TASK },
    validationSchema: taskFormSchema,
    onSubmit: (values) => saveForm(values),
  })

  return (
    <div className="w-screen">
      <main className="app-main">
        <div className="sticky top-0">
          <div className="app-min-max-w bg-app-light dark:bg-app-dark-900">
            <AppBreadcrumb />
            <AppHeader>
              <div className="flex justify-between items-center">
                <div>
                  <h1>Tasks</h1>
                  <p className="text-app-light-200 dark:text-gray-500">
                    There are {tasksState.length} total tasks.
                  </p>
                </div>
                <div className="flex items-center gap-2">
                  <div className="flex w-48 md:w-60 lg:w-96">
                    <AppInput
                      item={{
                        type: 'search',
                        name: 'search',
                        value: searchTerm,
                        placeholder: 'Search...',
                      }}
                      onChange={(e) => setSearchTerm(e.target.value)}
                    />
                    <button onClick={handleSearch} className="p-3 pl-2">
                      <MdOutlineSearch />
                    </button>
                  </div>
                  <AppButtonIcon
                    onClick={() => setShowForm(true)}
                    textBg={ButtonBgText.Purple}
                  >
                    <span className=" bg-app-light text-black inline-block px-2 py-2 rounded-full mr-4">
                      <PiPlus />
                    </span>
                    <span className="text-center">New Task</span>
                  </AppButtonIcon>
                </div>
              </div>
            </AppHeader>
            <div className="hidden md:block">
              <TasksListHeader
                handleClick={handleSortBy}
                sortState={sortState}
              />
            </div>
            <div className="block md:hidden">
              <TasksSortWithButton
                handleClick={handleSortBy}
                sortState={sortState}
              />
            </div>
          </div>
        </div>
        <section className="px-5">
          {tasksList.length > 0 && <TasksList list={tasksList} />}
          {tasksList.length <= 0 && <div>Empty list!</div>}
        </section>
      </main>
      {showForm && (
        <>
          <AppOverlay onClick={closeForm} />
          <div className="app-overlay-content-left lg:left-app-width-sidebar bg-app-light dark:bg-app-dark-900 rounded-br-md p-10 shadow-xl rounded-r-2xl">
            <TaskForm formik={formik}>
              <div className="flex gap-2 justify-end py-7">
                <AppButton onClick={closeForm} name="cancel">
                  Cancel
                </AppButton>
                <AppButton
                  onClick={formik.handleSubmit}
                  textBg={ButtonBgText.Purple}
                  name="submit"
                  disabled={formik.isSubmitting}
                >
                  Add Task
                </AppButton>
              </div>
            </TaskForm>
          </div>
        </>
      )}
    </div>
  )
}
