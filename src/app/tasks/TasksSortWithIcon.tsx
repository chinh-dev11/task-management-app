// TODO - SortWithIcon and SortWithButton could be refactor into a single Component.
import { SortIndex } from '@/types'
import cn from 'classnames'
import { ReactNode } from 'react'

interface SortWithIconProps {
  handleClick: (by: string) => void
  sortState: {
    endAt?: boolean
  }
  children?: ReactNode
  type?: string // icon || button
}

export default function SortWithIcon({
  handleClick,
  children,
  type = 'button',
  sortState,
}: SortWithIconProps) {
  const style = type === 'button' ? 'bg' : 'text'
  const textBgColor = sortState.endAt
    ? `${style}-app-light-200`
    : `${style}-app-light-100`

  return (
    <button
      onClick={() => handleClick(SortIndex.DueDate)}
      type="button"
      className={cn(textBgColor)}
    >
      {children}
    </button>
  )
}
