'use client'

import fetchData from '@/services/fetchData'
import { setTasksState } from '@/store/features/tasks/tasksSlice'
import { setCurrentUserState } from '@/store/features/usersSlice'
import { useAppSelector, useAppDispatch } from '@/store/hooks'
import { User } from '@/types'
import Image from 'next/image'
import { useRouter } from 'next/navigation'
import { ROUTES } from '@/const'

export default function Page() {
  const usersState: User[] = useAppSelector((state) => state.users.users)
  const dispatch = useAppDispatch()

  const router = useRouter()

  const authenticate = (user: User) => {
    const data = fetchData(user)
    dispatch(setTasksState(data))
    dispatch(setCurrentUserState(user))
    router.push(ROUTES.tasks.path) // TODO: redirect back to the origin url. Issue: how to track the origin url, before it gets redirected to login page when not authenticated.
  }

  return (
    <div className="w-screen">
      <main className="app-main">
        <div className="app-min-max-w bg-app-light dark:bg-app-dark-900 h-screen flex flex-col items-center justify-center ">
          <h2 className="mb-5">Login In with</h2>
          <div className="flex flex-col gap-2">
            {usersState.map((user) => (
              <div key={user.id}>
                <button onClick={() => authenticate(user)}>
                  <Image
                    src="/user-placeholder.png"
                    width={20}
                    height={20}
                    alt="avatar image"
                    priority={true}
                    className="inline-block rounded-full mr-2"
                  />
                  <span>{user.name}</span>
                </button>
              </div>
            ))}
          </div>
        </div>
      </main>
    </div>
  )
}
