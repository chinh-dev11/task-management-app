import { ROUTES } from '@/const'
import AppBreadcrumb from '@/components/AppBreadcrumb'
import AppHeader from '@/components/AppHeader'
import { MdArrowBackIosNew } from 'react-icons/md'

export default function Page() {
  return (
    <div className="w-screen">
      <main className="app-main">
        <div className="sticky top-0">
          <div className="app-min-max-w bg-app-light dark:bg-app-dark-900">
            <AppBreadcrumb link={ROUTES.home}>
              <div className="flex items-center gap-2">
                <span className="app-text-purple">
                  <MdArrowBackIosNew />
                </span>
                <span>Go Back to Home</span>
              </div>
            </AppBreadcrumb>
            <AppHeader>
              <div className="flex justify-between items-center">
                <div className="flex items-center gap-2">
                  <h4>404 - Page Not Found.</h4>
                </div>
              </div>
            </AppHeader>
          </div>
        </div>
      </main>
    </div>
  )
}
