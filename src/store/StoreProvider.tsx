'use client'

import { useRef, ReactNode } from 'react'
import { Provider } from 'react-redux'
import { setupStore, AppStore } from './store'

export default function StoreProvider({ children }: { children: ReactNode }) {
  const storeRef = useRef<AppStore | null>(null)
  if (!storeRef.current) {
    storeRef.current = setupStore()
  }

  return <Provider store={storeRef.current}>{children}</Provider>
}
