import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'
import data from '@/data/users.json'
import { User } from '@/types'

interface UserState {
  users: User[]
  currentUser: User | null
}

export const initialState: UserState = {
  users: data,
  currentUser: null,
}

export const usersSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {
    setUsersState: (state, action: PayloadAction<User[]>) => {
      state.users = action.payload
    },
    setCurrentUserState: (state, action: PayloadAction<User>) => {
      state.currentUser = action.payload
    },
  },
})

export const { setUsersState, setCurrentUserState } = usersSlice.actions
export const usersReducer = usersSlice.reducer
