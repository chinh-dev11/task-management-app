import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'
import { Task } from '@/types'

export const initialState = {
  value: <Task[]>[],
}

export const tasksSlice = createSlice({
  name: 'tasks',
  initialState,
  reducers: {
    setTasksState: (state, action: PayloadAction<Task[]>) => {
      state.value = action.payload
    },
  },
})

export const { setTasksState } = tasksSlice.actions
export const tasksReducer = tasksSlice.reducer
