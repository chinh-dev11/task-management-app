import { combineReducers, configureStore } from '@reduxjs/toolkit'
import { tasksReducer } from './features/tasks/tasksSlice'
import { usersReducer } from './features/usersSlice'

// Create the root reducer separately so we can extract the RootState type
const rootReducer = combineReducers({
  users: usersReducer,
  tasks: tasksReducer,
})

export const setupStore = (preloadedState?: Partial<RootState>) => {
  return configureStore({
    reducer: rootReducer,
    preloadedState,
  })
}

// Infer the type of setupStore
export type AppStore = ReturnType<typeof setupStore>
// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof rootReducer>
export type AppDispatch = AppStore['dispatch']
