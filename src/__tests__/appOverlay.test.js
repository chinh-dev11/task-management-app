import AppOverlay from '@/components/AppOverlay'
import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'

describe('AppOverlay', () => {
  it('renders the app overlay component', () => {
    render(<AppOverlay />)

    expect(screen.getByTestId('appOverlay')).toBeInTheDocument()
  })
})
