import { render, screen } from '@testing-library/react'
import TasksList from '@/app/tasks/TasksList'

describe('TasksList', () => {
  it('renders the task list', () => {
    const tasksList = [{ id: '1' }, { id: '2' }]
    render(<TasksList list={tasksList} />)

    const list = screen.getAllByTestId('listItem')
    expect(list.length).toEqual(2)
  })
})
