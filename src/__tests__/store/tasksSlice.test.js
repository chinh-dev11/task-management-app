import {
  tasksReducer,
  setTasksState,
  initialState,
} from '@/store/features/tasks/tasksSlice'

describe('tests for tasksSlice', () => {
  test('set tasks state', () => {
    const tasks = [
      {
        id: 'task-1235',
        title: 'Task 2',
        description: 'My second task.',
        createdAt: '2024-06-01',
        endAt: '2024-06-07',
        status: 'overdue',
        userId: '1234',
      },
    ]
    const tasksState = tasksReducer(initialState, setTasksState(tasks))
    expect(tasksState.value).toStrictEqual(tasks)
  })
})
