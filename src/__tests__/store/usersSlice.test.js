import {
  usersReducer,
  initialState,
  setCurrentUserState,
} from '@/store/features/usersSlice'

describe('tests for usersSlice', () => {
  test('initialize slice with initialValue', () => {
    const usersSliceInit = usersReducer(initialState, { type: 'unknown' })
    expect(usersSliceInit).toBe(initialState)
  })
  test('set current user action', () => {
    const currentUser = {
      id: '9999',
      permission: 'admin',
      name: 'Admin',
      username: 'admin',
    }
    const afterReducerOperation = usersReducer(
      initialState,
      setCurrentUserState(currentUser)
    )
    expect(afterReducerOperation.currentUser).toStrictEqual(currentUser)
  })
})
