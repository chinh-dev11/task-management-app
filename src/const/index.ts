export const DEFAULT_TASK = {
  id: '',
  title: '',
  description: '',
  createdAt: '',
  endAt: '',
  status: '',
  userId: '',
}
export const DEFAULT_USER = {
  id: '',
  permission: '',
  name: '',
  username: '',
}

export const ROUTES = {
  home: {
    key: 'home',
    path: '/',
  },
  login: {
    key: 'login',
    path: '/login',
  },
  tasks: {
    key: 'tasks',
    path: '/tasks',
  },
}
