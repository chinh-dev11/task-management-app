import data from '@/data/tasks.json'
import { User, Task, Permission } from '@/types'

/**
 * This could be a service request to an endpoint to get the user's tasks.
 * If admin: fetch all tasks.
 * Else: fetch only user's tasks.
 *
 * @param user
 * @returns
 */
export default function fetchData(user: User | null): Task[] {
  if (user?.permission === Permission.Admin) return data

  return data.filter(({ userId }) => userId === user?.id)
}
