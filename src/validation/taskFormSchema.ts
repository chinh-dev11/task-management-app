import * as Yup from 'yup'
import { todayDate } from '@/utils'

export const taskFormSchema = Yup.object().shape({
  title: Yup.string().required('required'),
  description: Yup.string()
    .required('required')
    .min(4, 'min 4 chars')
    .max(200, 'max 200 chars'),
  endAt: Yup.date()
    .required('required')
    .min(todayDate(), 'must be today or later'),
})
