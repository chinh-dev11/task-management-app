## Project

- [Repo](https://gitlab.com/chinh-dev11/task-management-app)
- [Hosting with Vercel](https://task-app.chinh.ca)
- Stack: Next, React, Redux, Typescript, Tailwind CSS, Jest, Yup, Formik.

#### TODO

- Check status at app start, set to Overdue if due date is before today.
- Add loading graphic while requesting data and/or route change.
- ~~Add page not found.~~
- Use database to persist the data, user creation and authentication (i.e. Firebase serverless).
- Add search behavior.
- Optimization
  - When possible with CSS, only one component for various screen sizes.
    - ie. TaskCard and TaskItem.
  - Create modal with Next and use <dialog> tag.
  - tasks list: add pagination / lazy loading.
- Add navigation.
- Add tests
  - Unit testing (Vitest)
  - Unit and Snapshot testing (Jest)
  - E2E testing (Playwright)
  - E2E and Component testing (Cypress)

#### To run test

```
npm run test
```

##

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `app/page.tsx`. The page auto-updates as you edit the file.

This project uses [`next/font`](https://nextjs.org/docs/basic-features/font-optimization) to automatically optimize and load Inter, a custom Google Font.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
